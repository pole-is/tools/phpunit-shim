# irstea/phpunit-shim - shim repository for phpunit/phpunit.

This package is a drop-in replacement for [phpunit/phpunit](https://phpunit.de/), which provides its PHAR archive as a binary.

It is built automatically from the official PHAR.

## Installation

	composer require irstea/phpunit-shim

or:

	composer require --dev irstea/phpunit-shim



## Usage

As you would use the original package, i.e. something like:

	vendor/bin/phpunit [options] [arguments]

## License

This distribution retains the license of the original software: BSD-3-Clause